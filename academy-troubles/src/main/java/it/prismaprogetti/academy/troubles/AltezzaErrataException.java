package it.prismaprogetti.academy.troubles;

public class AltezzaErrataException extends StampaException {

	public AltezzaErrataException() {
		super("altezza errata");
	}

}
