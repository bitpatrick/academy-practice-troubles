package it.prismaprogetti.academy.troubles;

public class SalvataggioAutomobileException extends Exception {

	public SalvataggioAutomobileException() {
		// TODO Auto-generated constructor stub
	}

	public SalvataggioAutomobileException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SalvataggioAutomobileException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SalvataggioAutomobileException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SalvataggioAutomobileException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
