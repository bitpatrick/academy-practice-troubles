package it.prismaprogetti.academy.troubles;

public class Stampante {

	private int numeroFogli;
	private int altezzaDimesioneStampa;
	private int larghezzaDimesioneStampa;

	public Stampante(int altezzaDimesioneStampa, int larghezzaDimesioneStampa, int numeroFogli) {
		super();
		this.numeroFogli = numeroFogli;
		this.altezzaDimesioneStampa = altezzaDimesioneStampa;
		this.larghezzaDimesioneStampa = larghezzaDimesioneStampa;
	}

	public void stampa(int larghezzaFoglio, int altezzaFoglio, int numeroDiCopie)
			throws LarghezzaErrataException,AltezzaErrataException,FogliInsufficentiException {

		if (larghezzaFoglio < larghezzaDimesioneStampa) {
			throw new LarghezzaErrataException();
		}

		if (altezzaFoglio < altezzaDimesioneStampa) {
			throw new AltezzaErrataException();
		}

		if (numeroFogli < numeroDiCopie) {
			throw new FogliInsufficentiException();
		}

	}
}
