package it.prismaprogetti.academy.troubles;

import java.io.FileNotFoundException;
import java.sql.SQLDataException;

import javax.xml.datatype.DatatypeConfigurationException;

public class Test {

	public static void main(String[] args) throws SQLDataException {

		try {

			try {
				throw new FileNotFoundException("Non trovo il file XYZ");

			} catch (FileNotFoundException ex) {

				throw new DatatypeConfigurationException("Fallita configurazione automobili", ex);

			}

		} catch (DatatypeConfigurationException e) {
			throw new SQLDataException("Impossibile eseguire il comando di aggiornamento", e);
		}

	}

}
