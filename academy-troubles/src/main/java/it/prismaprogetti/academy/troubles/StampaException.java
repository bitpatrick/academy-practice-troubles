package it.prismaprogetti.academy.troubles;

public class StampaException extends Exception {

	public enum Errore {
		ALTEZZA, LARGHEZZA, NUMERO_FOGLI;
	}

	protected StampaException(String causa) {
		super("Errore di stampa: " + causa);
	}

}
