package it.prismaprogetti.academy.troubles;

public class Main2 {

	public static void main(String[] args) throws Exception {

		Stampante stampante = new Stampante(10, 7, 5);

		int altezzaDimesioneStampa = 0;
		int larghezzaDimesioneStampa = 0;
		int numeroDiCopie = 10;
		
		int numeroDiTentativi=0;

		do {

			try {
				stampante.stampa(larghezzaDimesioneStampa, altezzaDimesioneStampa, numeroDiCopie);
				break;
			
			}catch (StampaException e) {
				System.err.println("Stampa fallita: "+ e.getMessage());
				numeroDiTentativi++;

				
				if(e instanceof FogliInsufficentiException ) {
					numeroDiCopie= numeroDiCopie-1;
					System.out.println("Regolo numero copie: "+larghezzaDimesioneStampa);
				}else if(e instanceof LarghezzaErrataException ) {
					larghezzaDimesioneStampa=larghezzaDimesioneStampa+1;
					System.out.println("Regolo larghezza a: "+larghezzaDimesioneStampa);
				}else {
					
					altezzaDimesioneStampa=altezzaDimesioneStampa+1;
					System.out.println("Regolo altezza a: "+altezzaDimesioneStampa);
				}
			
			}
			

		} while (true);

		System.out.println("Risolto in numero tentativi " + numeroDiTentativi);
	}


}
