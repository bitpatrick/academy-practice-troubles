package it.prismaprogetti.academy.troubles;

public class FogliInsufficentiException extends StampaException {

	public FogliInsufficentiException() {
		super("fogli insufficienti");
	}

}
