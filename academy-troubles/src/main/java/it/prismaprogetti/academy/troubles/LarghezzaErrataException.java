package it.prismaprogetti.academy.troubles;

public class LarghezzaErrataException extends StampaException {

	public LarghezzaErrataException() {
		super("larghezza errata");
	}

}
