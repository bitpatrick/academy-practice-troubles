package it.prismaprogetti.academy.troubles;

public class Main3 {

	public static void main(String[] args){

		Stampante stampante = new Stampante(10, 7, 5);

		int altezzaDimesioneStampa = 0;
		int larghezzaDimesioneStampa = 0;
		int numeroDiCopie = 10;

		int numeroDiTentativi = 0;

		do {

			try {

				try {
					stampante.stampa(larghezzaDimesioneStampa, altezzaDimesioneStampa, numeroDiCopie);
					break;
				} catch (StampaException ex) {
					System.err.println("Stampa fallita: " + ex.getMessage());
					numeroDiTentativi++;
					
					throw ex;
				}

			} catch (FogliInsufficentiException ex) {

				numeroDiCopie = numeroDiCopie - 1;
				System.out.println("Regolo numero copie: " + larghezzaDimesioneStampa);

			} catch (LarghezzaErrataException ex) {

				larghezzaDimesioneStampa = larghezzaDimesioneStampa + 1;
				System.out.println("Regolo larghezza a: " + larghezzaDimesioneStampa);

			} catch (AltezzaErrataException ex) {

				altezzaDimesioneStampa = altezzaDimesioneStampa + 1;
				System.out.println("Regolo altezza a: " + altezzaDimesioneStampa);
			} catch (StampaException e) {
				// non verificabile
			}

		} while (true);

		System.out.println("Risolto in numero tentativi " + numeroDiTentativi);
	}

}
