package it.prismaprogetti.academy.troubles;

public class Main {

	public static void main(String[] args) throws Exception {

		Stampante stampante = new Stampante(10, 7, 5);

		int altezzaDimesioneStampa = 0;
		int larghezzaDimesioneStampa = 0;
		int numeroDiCopie = 10;
		
		int numeroDiTentativi=0;

		do {

			try {
				stampante.stampa(larghezzaDimesioneStampa, altezzaDimesioneStampa, numeroDiCopie);
				break;
			
			} catch (FogliInsufficentiException ex) {
				System.err.println("Stampa fallita: "+ ex.getMessage());
				numeroDiTentativi++;
				
				numeroDiCopie= numeroDiCopie-1;
				System.out.println("Regolo numero copie: "+larghezzaDimesioneStampa);

				
			} catch (LarghezzaErrataException ex) {
				System.err.println("Stampa fallita: "+ ex.getMessage());
				numeroDiTentativi++;
				
				larghezzaDimesioneStampa=larghezzaDimesioneStampa+1;
				System.out.println("Regolo larghezza a: "+larghezzaDimesioneStampa);

			
			} catch (AltezzaErrataException ex) {
				System.err.println("Stampa fallita: "+ ex.getMessage());
				numeroDiTentativi++;
				
				altezzaDimesioneStampa=altezzaDimesioneStampa+1;
				System.out.println("Regolo altezza a: "+altezzaDimesioneStampa);
			}
			

		} while (true);

		System.out.println("Risolto in numero tentativi " + numeroDiTentativi);
	}


}
